var http = require('http');
var path = require('path');

var async = require('async');
var socketio = require('socket.io');
var express = require('express');
var randoms = require('randomstring');

var picker = require('./utils/picker');
var morgan = require('morgan');

var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use( morgan('[:date[clf]] - :status :method :remote-addr :url :response-time ms') );
router.use(express.static(path.resolve(__dirname, 'client')));
var messages = [];
var sockets = [];

io.on('connection', function (socket) {
    messages.forEach(function (data) {
      socket.emit('message', data);
    });

    sockets.push(socket);

    socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      updateRoster();
    });

    socket.on('message', function (msg) {
      var text = String(msg || '');

      if (!text)
        return;

      socket.get('name', function (err, name) {
        var data = {
          name: name,
          text: text
        };

        broadcast('message', data);
        messages.push(data);
      });
    });

    socket.on('identify', function (name) {
      socket.set('name', String(name || 'Anonymous'), function (err) {
        updateRoster();
      });
    });
  });

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      broadcast('roster', names);
    }
  );
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

var App = {
  config: {
    responseDistribution: {
      error: .25, // 500
      success: .55, //200
      notFound: .05, //404
      forbidden: .05, //403
      unauthorized: .05, //401
      timeOut: .05 //408
    }
  },
  setConfig: function( _config ){
    App.config = _config;
  },
  getConfig: function(req, res){
    res.send( App.config );
  },
  pickResponse: function(){
    var r = picker.pick();
    return( r );
  },
  getCode: function( option ){
    var codes = {
      error: 500,
      //success: 200,
      notFound: 404,
      forbidden: 403,
      unauthorized: 401,
      timeOut: 408
    },
    code = (option && codes[option]) ? codes[option] : 200;
    return(code);
  },
  initRoutes: function(){
    router.get("/api/token", function(req,res){
      var r = App.pickResponse(),
          code = App.getCode( r ),
          payload = (code === 200) ? {token: randoms.generate(15)} : {error: r};
      res.status( code ).send( payload );
    });
    router.route("/api/config").get( this.getConfig ).post( this.setConfig );
  },
  init: function(){
    picker.init( {values: this.config.responseDistribution} );
    this.initRoutes();
  }
}

App.init();

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
