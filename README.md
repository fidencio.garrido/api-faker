# API Faker

## About

API Faker enables testing of APIs returning random results.

## Use

### Running the server

``` bash
$ node server.js
```

You can use, and it is recommended, forever to keep the system alive.

``` bash
$ forever server.js
```

### Changing default configuration
At the moment, to change the configuration you will have to find the "config" section to change the probability of each event. The sum of the probabilities has to be exactly equals to 1.

``` javascript
"config": {
    "responseDistribution": {
      "error": .25, // 500
      "success": .55, //200
      "notFound": .05, //404
      "forbidden": .05, //403
      "unauthorized": .05, //401
      "timeOut": .05 //408
    }
```

Another required change will be in the response mapping, locate the `getCode` function and assign the custom HTTP error code. By default, if no code is specified for the value, the system will return 200.

``` javascript
var codes = {
      "error": 500,
      //success: 200,
      "notFound": 404,
      "forbidden": 403,
      "unauthorized": 401,
      "timeOut": 408
    },
```