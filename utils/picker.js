var _ = require("lodash");
    
var Picker = {
    _limits: [],
    pick: function(){
        var ev = null,
            limits = this.getLimits(),
            val = this.genRandom();
      for (var i=0; i<limits.length; i++){
        if (val> limits[i].min && val<limits[i].max){
          ev = limits[i].ev;
          break;
        }
      }
      return(ev);  
    },
    genRandom: function(){
        return( Math.random() );
    },
    getLimits: function(){
      return( this._limits );  
    },
    setLimits: function( values ){
        var prob = [],
            climit = 0,
            keys = Object.keys( values );
        for (var i=0; i<keys.length; i++){
            var _max = climit + values[keys[i]],
                lim = {ev: keys[i], min: climit, max: _max};
            climit = _max + .0001;
            prob.push( lim );
        }
        console.dir( prob );
        this._limits = prob;
    },
    init: function( options ){
      if ( typeof options!= "undefined" && _.isPlainObject(options) ){
          this.setLimits( options.values );
      } else{
          this.setLimits({});
      }
    }
};

module.exports = Picker;